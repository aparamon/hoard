# HOARD: Human Oriented Allotrope Resource Dump

It is unfortunate that Allotrope Data Format currently stores Data Description inside HDF5 file in an opaque binary form. To extract Data Description, user must resort to Allotrope Framework API, which is at this point only available for Java and .NET . Users of other programming environments, for example Python, are left behind, albeit Python being [a popular language of choice in pharma industry](https://www.python.org/about/success/astra).

But even if you are able to read in the RDF triples list, its natural representation is far from being human readable: resource identifiers are cryptic, the linear sequence of triples doesn't resemble the hierarchical structure of data, etc. Typically, your ADF triples list looks something like
````
urn:dc:spectra:ms:data:0 http://purl.allotrope.org/ontologies/result#AFR_0000285 4f0d2461:15e858fa77f:-7fb1
urn:dc:spectra:ms:data:0 http://purl.allotrope.org/ontologies/result#AFR_0000480 4f0d2461:15e858fa77f:-7fb6
urn:dc:spectra:ms:data:0 http://purl.allotrope.org/ontologies/result#AFR_0000512 http://purl.allotrope.org/ontologies/process#AFP_0001609
urn:dc:spectra:ms:data:0 http://purl.allotrope.org/ontologies/result#AFR_0000131 1^^http://www.w3.org/2001/XMLSchema#int
urn:dc:spectra:ms:data:0 http://purl.allotrope.org/ontologies/result#AFR_0000483 http://purl.allotrope.org/ontologies/result#AFR_0000320
urn:dc:spectra:ms:data:0 http://purl.allotrope.org/ontologies/result#AFR_0000215 urn:dc:spectra:ms:data:0:BasePeak
urn:dc:spectra:ms:data:0 http://purl.allotrope.org/ontologies/result#AFR_0000074 4f0d2461:15e858fa77f:-7fb2
````
which is hardly comprehensible.

In attachment you will find a small script that reads ADF file and dumps its Data Description in a human-oriented text form. It applies the following transformations:
* Statements for one subject are grouped together, similar to Turtle format.
* Anonymous resources are inlined, similar to Turtle; so are the resources that are only referenced once.
* Resource identifiers are replaced with prefLabel; typing information is stripped away from literals.
* The script also demonstrates how Allotrope Framework API can be used from Python.

Sample output:
````
urn:dc:spectra:ms:data:0 :
    -type- single-stage mass spectrum
    -m/z- :
        -type- range
        -minimum value- 118.805532294271
        -maximum value- 1010.088681742826
        -range unit- MassPerCharge
    -measurement time- :
        -numericValue- 0.0064970165
        -unit- MinuteTime
    -applied spectrum combination- no combination
    -ms level- 1
    -spectrum representation- profile spectrum
    -base peak- urn:dc:spectra:ms:data:0:BasePeak
        -type- peak
        -m/z- :
            -numericValue- 149.0238122
            -unit- MassPerCharge
        -intensity- :
            -numericValue- 2993424.8
            -unit- Number
    -total ion current- :
        -numericValue- 9623094.0
        -unit- Number
...
````

### Installation

1. Get [JRE](https://www.java.com/download/help/download_options.xml).
2. Download and extract ADF Library.
3. Pull in ADF Library Java dependencies using [maven](https://maven.apache.org): \
`> mvn dependency:copy-dependencies -DoutputDirectory=classes`
4. Install [HDF5 libraries](https://www.hdfgroup.org/hdf5). On Windows, make sure they are on `PATH` (or copy them near file **hoard.py**).
5. Download Allotrope Foundation Ontologies. Extract \*.ttl files to **ontologies** dir.
6. Install [JPype](https://pypi.org/project/JPype1). [Anaconda](https://www.anaconda.com) provides easy way to get the binaries if you are on Windows. You may need to specify `JAVA_HOME`, see [JPype installation manual](http://jpype.readthedocs.io/en/latest/install.html) for details. \
Your setup should look like:
````
classes
ontologies
    hoard.py
    hdf5.dll
    zlib.dll
    szip.dll
````

7. Run \
`> hoard.py path-to-dataset.adf` \
and get human-readable Data Description on your console, or \
`> hoard.py -h` \
for additional options.

### TODO

* Output normalization (logical ordering, diff)
* HTML output option

Enjoy!
