# Human Oriented Allotrope Resource Dump
# by Andrey Paramonov, ACD/Labs

import sys
import argparse

parser = argparse.ArgumentParser(
    description = 'HOARD: Human Oriented Allotrope Resource Dump.')
parser.add_argument('filename', type = str,
                    help = 'ADF or RDF file name')
parser.add_argument('-o', '--outfile', type = argparse.FileType('w+'), default = sys.stdout,
                    help = 'output file name (default stdout)')
parser.add_argument('-t', '--type', choices = ['text', 'ttl', 'dot', 'pdf', 'svg', 'png', 'cxl'],
                    help = 'output type/format (default text)')
parser.add_argument('-f', '--filter', type = str, help = 'filtering SPARQL query name/file name')
parser.add_argument('-p', '--param', action = 'append', help = 'query parameter (name=value)')
parser.add_argument('-r', '--raw', action = 'store_true',
                    help = 'output raw triples')
args = parser.parse_args()
if not args.type:
    outfile = args.outfile.name.lower()
    if outfile.endswith('.ttl'):
        args.type = 'ttl'
    elif outfile.endswith('.dot'):
        args.type = 'dot'
    elif outfile.endswith('.pdf'):
        args.type = 'pdf'
    elif outfile.endswith('.svg'):
        args.type = 'svg'
    elif outfile.endswith('.png'):
        args.type = 'png'
    elif outfile.endswith('.cxl'):
        args.type = 'cxl'
    else:
        args.type = 'text'

# TODO:
# * Normalization
# * Coloration

import os, os.path
from jpype import *
from functools import *
from itertools import *
import subprocess
from zipfile import ZipFile
import string
import hashlib
import math
import re

def getfiles(dir):
    for f in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, f)):
            yield os.path.join(dir, f)

scriptdir = os.path.dirname(os.path.abspath(__file__))
startJVM(getDefaultJVMPath(), '-ea',
         '-Djava.class.path=' + os.pathsep.join(getfiles(os.path.join(scriptdir, 'classes'))),
         '-Djava.library.path=' + scriptdir)
org = JPackage('org')
com = JPackage('com')

def Path(s):
    return java.nio.file.Paths.get(s, [])

# load file
if args.filename.endswith('.adf'):
    adfService = org.allotrope.adf.service.AdfServiceFactory.create()
    adfFile = adfService.openFile(Path(args.filename), True)
    dataDescription = adfFile.getDataDescription()
else:
    dataDescription = com.hp.hpl.jena.rdf.model.ModelFactory.createDefaultModel()
    dataDescription.read('file:' + args.filename)

if args.filter:
    rqueryfile = os.path.join(os.path.dirname(__file__),
                              'queries', args.filter + '.sparql')
    if os.path.isfile(args.filter) or not os.path.isfile(rqueryfile):
        queryfile = open(args.filter)
    else:
        queryfile = open(rqueryfile)
    print('Using filtering query {}'.format(os.path.basename(queryfile.name)), file = sys.stderr)
    query = com.hp.hpl.jena.query.ParameterizedSparqlString(queryfile.read())
    if args.param:
        for param in args.param:
            (name, value) = param.split('=', maxsplit = 1)
            query.setLiteral(name, value)
    model = com.hp.hpl.jena.query.QueryExecutionFactory.create(query.asQuery(),
                                                               dataDescription).execConstruct();
else:
    model = dataDescription

# construct ontology
ontology = com.hp.hpl.jena.rdf.model.ModelFactory.createOntologyModel()
ontology.add(model)

docManager = com.hp.hpl.jena.ontology.OntDocumentManager.getInstance()
nsre = re.compile('\@prefix\s+\:\s+\<(.+)\>\s+\.')
for ttlfile in getfiles(os.path.join(scriptdir, 'ontologies')):
    namespace = nsre.search(open(ttlfile, encoding = 'utf8').readline()).group(1).strip('#')
    docManager.addAltEntry(namespace, 'file:' + ttlfile.replace(os.sep, '/'))
for ttlfile in getfiles(os.path.join(scriptdir, 'ontologies')):
    namespace = nsre.search(open(ttlfile, encoding = 'utf8').readline()).group(1).strip('#')
    docManager.loadImport(ontology, namespace)

label = ontology.getProperty('http://www.w3.org/2000/01/rdf-schema#label')
prefLabel = ontology.getProperty('http://www.w3.org/2004/02/skos/core#prefLabel')
inverseOf = ontology.getProperty('http://www.w3.org/2002/07/owl#inverseOf')

# dump
def stripnp(func):
   def wrapper(*args, **kwargs):
       return ''.join(filter(lambda x: x in string.printable, func(*args, **kwargs)))
   return wrapper

@stripnp
@lru_cache()
def getlabel(node):
    if node.isLiteral():
        return str(node.asLiteral().getValue())
    else:
        resource = ontology.getResource(node.getURI())
        labelprop = resource.getProperty(prefLabel) or resource.getProperty(label)
        if labelprop:
            return str(labelprop.getObject())
        elif model.contains(node, None):
            return str(node)
        else:
            return node.getLocalName()

def getreflabel(node, parents):
    result = None
    for parent in reversed(parents):
        result = result + '/..' if result else '..'
        if parent == node:
            return '<' + result + '>'

def getrefs(node, mutual = True):
    for stmt in model.listStatements(None, None, node):
        subj, pred = stmt.getSubject(), stmt.getPredicate()
        if not mutual:
            p = ontology.getResource(pred.getURI()).getProperty(inverseOf)
            if not p:
                continue
            invpred = model.getProperty(p.getObject().getURI())
            if ontology.contains(node, invpred, subj):
                continue
        yield stmt

@lru_cache()
def isqanon(node):
    'Is quasi-anonymous resource (should be inlined)'
    if node.isResource():
        n = len(list(islice(getrefs(node), 2)))
        return n == 1 or \
            n == 0 and len(list(islice(getrefs(node, False), 2))) == 1

@lru_cache()
def nodeid(node):
    return 'n' + hashlib.blake2s(str(node).encode('utf-8'), digest_size = 16).hexdigest()

@lru_cache()
def degree(node):
    if node.isLiteral():
        return 1
    indegree = len(list(model.listStatements(None, None, node)))
    outdegree = len(list(model.listObjectsOfProperty(node.asResource(), None)))
    return indegree + outdegree

def dump(subj, parents = list()):
    for prop in subj.listProperties():
        subj, pred, obj = prop.getSubject(), prop.getPredicate(), prop.getObject()
        if args.raw:
            print('{}{} {} {}'.format(' '*4*len(parents), subj, pred, obj), file = args.outfile)
        print(' '*4*(len(parents) + 1) + '-' + getlabel(pred) + '- ' +
              (':' if obj.isAnon() else getreflabel(obj, parents) or getlabel(obj)), file = args.outfile)
        if (obj.isAnon() or isqanon(obj)) and obj not in parents:
            dump(obj, parents + [subj])
    if not len(parents):
        print(file = args.outfile)

def layout(model, format, outfile):
    dot = subprocess.Popen(['neato',
                            '-T' + format,
                            '-Goutputorder=edgesfirst',
                            '-Goverlap=vpsc',
                            '-GK=100',
                            '-Nstyle=filled',
                            '-Nfillcolor=white',
                            '-Nfontname=Sans',
                            '-Npenwidth=0.5',
                            '-Efontname=Sans',
                            '-Efontsize=10'],
                           stdin = subprocess.PIPE,
                           stdout = outfile,
                           universal_newlines = True)
    print('digraph graphname {', file = dot.stdin)
    nodes = set()
    literalnum = 0
    for stmt in model.listStatements():
        subj, pred, obj = stmt.getSubject(), stmt.getPredicate(), stmt.getObject()
        if obj.isLiteral():
            literalnum += 1
            objid = 'n' + str(literalnum)
        else:
            objid = nodeid(obj)
        for node in (subj, obj):
            if node.isLiteral():
                assert(node == obj)
                print(objid, '[label="{}" shape=rectangle fontsize="{}"]'.format(
                    getlabel(node).replace('"', r'\"'),
                    str(12*math.log(1 + degree(node), 2))), file = dot.stdin)
            elif node not in nodes:
                print(nodeid(node), '[label="{}" shape={} fontsize="{}"]'.format(
                    '' if node.isAnon() else getlabel(node).replace('"', r'\"'),
                    'point' if node.isAnon() else 'ellipse',
                    str(12*math.log(1 + degree(node), 2))), file = dot.stdin)
                nodes.add(node)
        print('{} -> {} [label="{}" len={}]'.format(
            nodeid(subj), objid, getlabel(pred).replace('"', r'\"'),
            str(0.5*(degree(subj)**2 + degree(obj)**2)**(1/2))), file = dot.stdin)
    print('}', file = dot.stdin)
    dot.stdin.close()
    return dot

print('Output type is {}'.format(args.type), file = sys.stderr)
if args.type == 'text':
    subjs = set()
    for stmt in model.listStatements():
        subj = stmt.getSubject()
        if subj not in subjs:
            if not isqanon(subj):
                print(('<anon>' if subj.isAnon() else getlabel(subj)) + ' :',
                      file = args.outfile)
                dump(subj)
            subjs.add(subj)

elif args.type in ('dot', 'pdf', 'svg', 'png'):
    dot = layout(model, args.type, args.outfile)
    dot.wait()

elif args.type == 'cxl':
    import pydot
    from lxml import etree

    coords = dict()
    dot = layout(model, 'dot', subprocess.PIPE)
    graph = pydot.graph_from_dot_data(dot.stdout.read())[0]
    (xoff, yoff, *rest) = tuple(float(v) for v in graph.get_bb().strip('"').split(','))
    for node in graph.get_nodes():
        if node.get_pos():
            (x, y) = tuple(float(v) for v in node.get_pos().strip('"').split(','))
            coords[node.get_name()] = (10 + x - xoff, 10 + y - yoff)

    root = etree.Element('cmap',
                         nsmap = {None: 'http://cmap.ihmc.us/xml/cmap/',
                                  'dc': 'http://purl.org/dc/elements/1.1/',
                                  'dcterms': 'http://purl.org/dc/terms/',
                                  'vcard': 'http://www.w3.org/2001/vcard-rdf/3.0#'})
    mapnode = etree.SubElement(root, 'map')
    conceptList = etree.SubElement(mapnode, 'concept-list')
    linkingPhraseList = etree.SubElement(mapnode, 'linking-phrase-list')
    connectionList = etree.SubElement(mapnode, 'connection-list')
    conceptAppearanceList = etree.SubElement(mapnode, 'concept-appearance-list')
    linkingPhraseAppearanceList = etree.SubElement(mapnode, 'linking-phrase-appearance-list')
    connectionAppearanceList = etree.SubElement(mapnode, 'connection-appearance-list')

    nodes = set()
    literalnum = 0
    for stmt in model.listStatements():
        subj, pred, obj = stmt.getSubject(), stmt.getPredicate(), stmt.getObject()
        if obj.isLiteral():
            literalnum += 1
            objid = 'n' + str(literalnum)
        else:
            objid = nodeid(obj)
        for node in (subj, obj):
            if node.isLiteral():
                assert(node == obj)
                elem = etree.SubElement(conceptList, 'concept')
                appelem = etree.SubElement(conceptAppearanceList, 'concept-appearance')
                elem.set('label', getlabel(node))
                elem.set('id', objid)
            elif node not in nodes:
                if node.isAnon():
                    elem = etree.SubElement(linkingPhraseList, 'linking-phrase')
                    appelem = etree.SubElement(linkingPhraseAppearanceList, 'linking-phrase-appearance')
                else:
                    elem = etree.SubElement(conceptList, 'concept')
                    appelem = etree.SubElement(conceptAppearanceList, 'concept-appearance')
                    elem.set('label', getlabel(node))
                elem.set('id', nodeid(node))
                nodes.add(node)
            else:
                elem = None
            if elem is not None:
                appelem.set('id', elem.get('id'))
                (x, y) = coords[elem.get('id')]
                appelem.set('x', str(int(round(x))))
                appelem.set('y', str(int(round(y))))

        linkid = nodeid(subj) + '-' + nodeid(pred) + '-' + objid

        elem = etree.SubElement(linkingPhraseList, 'linking-phrase')
        elem.set('label', getlabel(pred))
        elem.set('id', linkid)
        appelem = etree.SubElement(linkingPhraseAppearanceList, 'linking-phrase-appearance')
        appelem.set('id', linkid)
        (subjx, subjy) = coords[nodeid(subj)]
        (objx, objy) = coords[objid]
        appelem.set('x', str(int(round(0.5*(subjx + objx)))))
        appelem.set('y', str(int(round(0.5*(subjy + objy)))))

        elem = etree.SubElement(connectionList, 'connection')
        elem.set('id', 'c1:' + linkid)
        elem.set('from-id', nodeid(subj))
        elem.set('to-id', linkid)
        appelem = etree.SubElement(connectionAppearanceList, 'connection-appearance')
        appelem.set('id', 'c1:' + linkid)
        appelem.set('arrowhead', 'no')

        elem = etree.SubElement(connectionList, 'connection')
        elem.set('id', 'c2:' + linkid)
        elem.set('from-id', linkid)
        elem.set('to-id', objid)
        appelem = etree.SubElement(connectionAppearanceList, 'connection-appearance')
        appelem.set('id', 'c2:' + linkid)
        appelem.set('arrowhead', 'yes')

    #with ZipFile(args.outfile.buffer, 'w') as f:
    #    f.writestr('cmap', etree.tostring(root, xml_declaration = True, encoding = 'utf-8', pretty_print = True))
    etree.ElementTree(root).write(args.outfile.buffer,
                                  xml_declaration = True, encoding = 'utf-8', pretty_print = True)

else:
    raise NotImplementedError()

shutdownJVM()
